import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";

import { DragulaService } from 'ng2-dragula';
import { AuthService } from './shared/auth/auth.service';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { JwtInterceptor } from './shared/auth/jwt.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import {EmployeeService} from './shared/services/employee.service';
import {CompanyService} from './shared/services/company.service';
import {DeviceService, } from './shared/services/device.service';
import {TimeSheetService, } from './shared/services/time-sheet.service';
import {TransactionService} from './shared/services/transaction.service';
import {UserService} from './shared/services/user.service';
import {NotificationService} from './shared/services/notification.service';

import {WebSocketService} from './shared/services/webSocket.service';

import * as $ from 'jquery';



export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }

@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        ContentLayoutComponent
    ],
    imports: [
        BrowserAnimationsModule,
        StoreModule.forRoot({}),
        AppRoutingModule,
        SharedModule,
        FormsModule,
        HttpClientModule,
        ToastrModule.forRoot(),
        NgbModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
              }
        }),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBr5_picK8YJK7fFR2CPzTVMj6GG1TtRGo'
        })
    ],
    providers: [
        AuthService,
        AuthGuard,
        DragulaService,
        EmployeeService,
        CompanyService,
        UserService,
        TimeSheetService,
        TransactionService,
        DeviceService,
        NotificationService,
        WebSocketService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true,
          },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }