﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {TimeSheet} from '../models/time-sheet';

import {config} from '../../config';

@Injectable()
export class TimeSheetService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<TimeSheet[]>(config.backend + '/api/timeSheet/all');
  }

  getAllByEmployee(employeeId: number) {
    return this.http.get<TimeSheet[]>(config.backend + '/api/timeSheet/employee/' + employeeId);
  }

  getById(id: number) {
    return this.http.get(config.backend + '/api/timeSheet/' + id);
  }

  create(timeSheet: TimeSheet) {
    return this.http.post(config.backend + '/api/timeSheet', timeSheet);
  }

    createMultiple(timeSheets: TimeSheet[]) {
      return this.http.post<TimeSheet[]>(config.backend + '/api/timeSheet/createMultiple', timeSheets);
    }

  update(timeSheet: TimeSheet) {
    return this.http.put(config.backend + '/api/timeSheet/' + timeSheet.id, timeSheet);
  }

  delete(id: number) {
    return this.http.delete(config.backend + '/api/timeSheet/' + id);
  }
}
