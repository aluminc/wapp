﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Device} from '../models';

import {config} from '../../config';

@Injectable()
export class DeviceService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Device[]>(config.backend + '/api/device/all');
  }

  getAllByCompany(companyId: number) {
    return this.http.get<Device[]>(config.backend + '/api/device/company/' + companyId);
  }

  getById(id: number) {
    return this.http.get(config.backend + '/api/device/' + id);
  }

  create(device: Device) {
     return this.http.post(config.backend + '/api/device', device);
  }

  update(device: Device, id:number) {
    return this.http.put(config.backend + '/api/device/' + id, device);
  }

  delete(id: number) {
    return this.http.delete(config.backend + '/api/device/' + id);
  }


}
