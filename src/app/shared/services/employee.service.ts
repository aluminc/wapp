﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Employee} from '../models';
import {config} from '../../config';

@Injectable()
export class EmployeeService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Employee[]>(config.backend + '/api/employee/all');
  }

  getAllByCompany(companyId: number) {
    return this.http.get<Employee[]>(config.backend + '/api/employee/company/' + companyId);
  }

  getAllById(id: number) {
    return this.http.get<Employee[]>(config.backend + '/api/employee/id/' + id);
  }

  getById(id: number) {
    return this.http.get<Employee>(config.backend + '/api/employee/' + id);
  }

  create(employee: Employee) {
    return this.http.post<Employee>(config.backend + '/api/employee', employee);
  }

  createMultiple(employees: Employee[]) {
    return this.http.post<Employee[]>(config.backend + '/api/employee/createMultiple', employees);
  }

  update(employee: Employee) {
    return this.http.put(config.backend + '/api/employee/' + employee.id, employee);
  }

  delete(id: number) {
    return this.http.delete(config.backend + '/api/employee/' + id);
  }
}
