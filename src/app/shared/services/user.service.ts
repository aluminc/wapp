﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../models';

import {config} from '../../config';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<User[]>(config.backend + '/api/user/all');
  }

  getById(id: number) {
    return this.http.get(config.backend + '/api/user/' + id);
  }

  create(user: User, userType: number) {
    user.userType = userType;
 	return this.http.post(config.backend + '/register', user);
  }

  update(user: User) {
    return this.http.put(config.backend + '/api/user/' + user.id, user);
  }

  delete(id: number) {
    return this.http.delete(config.backend + '/api/user/' + id);
  }
}
