﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Company} from '../models';

import {config} from '../../config';

@Injectable()
export class CompanyService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Company[]>(config.backend + '/api/company/all');
  }

  getById(id: number) {
    return this.http.get(config.backend + '/api/company/' + id);
  }

  create(company: Company) {
     return this.http.post(config.backend + '/api/company', company);
  }

  update(company: Company, id:number) {
    return this.http.put(config.backend + '/api/company/' + id, company);
  }

  delete(id: number) {
    return this.http.delete(config.backend + '/api/company/' + id);
  }


}
