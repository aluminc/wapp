import {Injectable} from "@angular/core";
import {config} from '../../config';

declare var require: any;
 var SockJs = require("sockjs-client");
 var Stomp = require("@stomp/stompjs");

@Injectable()
export class WebSocketService {

    // Open connection with the back-end socket
    public connect() {
      //  let socket = new SockJs(`http://localhost:8080/socket`);
        let socket = new SockJs(config.backend + `/socket`);

        let stompClient = Stomp.over(socket);
        return stompClient;
    }
}