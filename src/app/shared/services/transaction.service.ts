﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Transaction} from '../models';

import {config} from '../../config';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Transaction[]>(config.backend + '/api/transaction/all');
  }

  getById(id: number) {
    return this.http.get(config.backend + '/api/transaction/' + id);
  }

  create(transaction: Transaction) {
    return this.http.post(config.backend + '/api/transaction', transaction);
  }

  update(transaction: Transaction) {
    return this.http.put(config.backend + '/api/transaction/' + transaction.id, transaction);
  }

  delete(id: number) {
    return this.http.delete(config.backend + '/api/transaction/' + id);
  }
}
