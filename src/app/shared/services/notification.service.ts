import {Injectable, EventEmitter, Output} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Notifications } from '../models/notifications';
import {HttpClient} from '@angular/common/http';
import {config} from '../../config';

@Injectable()
export class NotificationService {

    redirect: EventEmitter<Notifications> = new EventEmitter();
    currentUser = JSON.parse(localStorage.getItem('currentUser'));

    constructor(public toastr: ToastrService,
                private http: HttpClient) {
    }
    
    notify(notifications: any){
            let n = JSON.parse(notifications.body);
            if(this.currentUser.id === n.userId){
                this.redirect.emit()
            }                
    }

    refresh(){
        return this.redirect;
    }

    getAllByUser(userId: number) {
        return this.http.get<Notifications[]>(config.backend + '/api/notifications/user/' + userId);
    }

    setSeen(id: number) {
        return this.http.get<Notifications>(config.backend + '/api/notifications/seen/' + id);
    }

    setSeenByUser(userId: number) {
        return this.http.get<Notifications>(config.backend + '/api/notifications/seen/user/' + userId);
    }
}