import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../auth/auth.service';
import { Notifications } from '../models/notifications';
import { NotificationService } from '../services/notification.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
    nCount;
    notifications:Notifications[] = [];
    currentLang = 'en';
    toggleClass = 'ft-maximize';

    currentUser = JSON.parse(localStorage.getItem('currentUser'));
    subscription: any;

    constructor(public translate: TranslateService,
                public auth: AuthService,
                private notificationService: NotificationService) {
        const browserLang: string = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : 'en');
    }

    ngOnInit(){
        this.loadAllNotifications();
        this.subscription = this.notificationService.refresh()
            .subscribe(item => this.loadAllNotifications());
    }

    ChangeLanguage(language: string) {
        this.translate.use(language);
    }

    printDateTime(dateValue: string) {
        if (dateValue === null) {
           return '...';
        } else {
            let date = new Date(dateValue );
  
            return ('0' + (date.getUTCHours())).slice(-2) + ':' + ('0' + (date.getUTCMinutes())).slice(-2);
        }
    }

    MarkAsSeen(notifications: Notifications, i: number){
        this.notificationService.setSeen(notifications.id).subscribe( 
            data => {
                this.loadAllNotifications();
                // this.notifications.splice(this.nCount-i+1,1);
                // console.log(i);
                // this.nCount = this.notifications.length;
            }, 
            error => {console.log("error: " + error);}
        );
        
    }

    MarkAllAsSeen(){
        if(this.notifications.length>0){
            let n = this.notifications[0];
            this.notificationService.setSeenByUser(n.userId).subscribe( 
                data => {this.loadAllNotifications();}, 
                error => {console.log("error: " + error);}
            );
        }

    }

    ToggleClass() {
        if (this.toggleClass === 'ft-maximize') {
            this.toggleClass = 'ft-minimize';
        }
        else
            this.toggleClass = 'ft-maximize';
    }

    logout(){
        this.auth.logout;
    }

    loadAllNotifications(){
        this.notificationService.getAllByUser(this.currentUser.id).subscribe(notifications => {
             this.notifications = notifications;
             this.nCount = this.notifications.length;
             });
    }



}
