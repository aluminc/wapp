import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {config} from '../../config';
import { Router, ActivatedRoute } from "@angular/router";
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService {
  token: string;

  constructor(private http: HttpClient,
              private router: Router,
              private route: ActivatedRoute) {}

  signupUser(email: string, password: string) {
    //your code for signing up the new user
  }

  signinUser(email: string, password: string) {
    return this.http.post<any>(config.backend + '/login', {email: email, password: password})
      .map(user => {
        if (user) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.token = user.token;
        }

        return user;
      });
  }

  logout() {   
    localStorage.removeItem('currentUser');
    this.token = null;
    this.router.navigate(['/pages/login']); 
  }

  getToken() {    
    return this.token;
  }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token 
    return true;
  }
}
