export class Notifications {
    id: number;
    userId: number;
    message: string;
    dateTime: string;
    code: string;
    seen: boolean;
    type: string;
}