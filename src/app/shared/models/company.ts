export class Company {
    id: number;
    emailAddress: string;
    phone: string;
    address: string;
    city: string;
    state: string;
    contact: string;
    fax: string;
    zipCode: string;
}
