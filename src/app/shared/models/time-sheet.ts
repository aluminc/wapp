export class TimeSheet {
  id: number;
  employeeId: number;
  employeeFullName: string;
  startTime: string;
  finishTime: string;
  comment: string;
}
