﻿export class Transaction {
  id: number;
  companyId: number;
  deviceId: number;
  cardId: number;
  employeeId: number;
  status: number;
  cpuId: string;
  rfId: string;
  transactionType: string;
  logNum: string;
  dateTime: string;
}
