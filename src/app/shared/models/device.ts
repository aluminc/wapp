export class Device {
    id: number;
    cpuId: string;
    companyId: number;
    status: number;
    lastLogTime: string;
    description: string;
    alive: boolean;
}
