import {TimeSheet} from './time-sheet';
export class EmployeeTimeSheet {
    id: number;
    team_name: string;
    fullname: string;
    primary_colour: string;
    is_clocked_in: number;
    lightness: number;
    current_timesheet?: TimeSheet;
    last_timesheet?: TimeSheet;
}
