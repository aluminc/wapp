import { Timestamp } from "rxjs";

export class Employee {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  title: string;
  email: string;
  phone: string;
  cardId: number;
  companyId: number;
  status: string;
  pwd: string;
  lastClockIn: number;
  lastClockOut: number;
  sendInvitation: boolean;
  active: boolean;
  onlineFor: number;
}

