import { Component, OnDestroy, OnInit, OnChanges, Input, Output  } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { EmployeeService } from '../shared/services/employee.service';

import { Employee } from '../shared/models/employee';

@Component({
    selector: 'app-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.scss']
})


export class EmployeeListComponent implements OnDestroy, OnInit, OnChanges{

    employees: Employee[] = [];
    currentUser = JSON.parse(localStorage.getItem('currentUser'));
    isDelete ;

    constructor(private employeeService: EmployeeService,
                private toastr: ToastrService) {
    }

    settings = {
        actions: { delete: this.isDelete},
      
        attr: {
            class: "table table-responsive"
          },
          add: {
            createButtonContent: '<i class="ft-check success font-medium-1 mr-2"></i>',
            cancelButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
            confirmCreate: true,   
          },
          edit:{
            editButtonContent: '<i class="ft-edit-2 info font-medium-1 mr-2"></i>',
            saveButtonContent: '<i class="ft-check success font-medium-1 mr-2"></i>',
            cancelButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
            confirmSave: true,
          },
          delete:{
            deleteButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
            confirmDelete: true,
          },
      columns: {
        firstName: {
          title: 'First Name',
          type: 'string',
        },
        lastName: {
          title: 'Last Name',
          type: 'string',
        },
        title: {
          title: 'Title',
          type: 'string',
        },
        email: {
          title: 'E-Mail',
          type: 'string',
        },
        phone: {
          title: 'Phone',
          type: 'string',
        },
        cardId: {
          title: 'Card Id',
          type: 'string',
        },
        active: {
          title: 'Active',
          valuePrepareFunction: (value) => { if (value) { this.isDelete = true; return  'Active' } else { this.isDelete = false; return  'Deleted' } },
          type: 'html',
          editor: {type: 'list',
            config: {
                list: [
                  { value: 'true', title: 'Active' },
                  { value: 'false', title: 'Deleted' },
                  ],
            }},
          filter: {
            type: 'list',
            config: {
                selectText: 'Show all',
                list: [
                  { value: 'true', title: 'Active' },
                  { value: 'false', title: 'Deleted' },
                  ],
            },
          },
        },
      },
    };

    ngOnInit() {
        this.loadAllEmployees();
     }

     ngOnChanges() {
      //  this.loadAllEmployees();
      }
  
    private loadAllEmployees() {
        this.employeeService.getAllByCompany(this.currentUser.companyId).subscribe(employees => { this.employees = employees; });
    }
  
     ngOnDestroy() {
     }

     printDateTime(dateValue: string) {
        if (dateValue === null) {
           return '...';
        } else {
            let date = new Date(dateValue );
  
            return ('0' + (date.getUTCHours())).slice(-2) + ':' + ('0' + (date.getUTCMinutes())).slice(-2);
        }
    }

    onEmployeeChange(e) {
        this.toastr.success(e.toString, '');
      }

    onDeleteConfirm(event): void {
        if (window.confirm('Are you sure you want to deactivate the employee?')) {
          let employee: any = event.data;
          employee.active =false;
          this.employeeService.update(employee).subscribe(
              data => {
                  this.toastr.success('Employee deactiavated', employee.firstName + ' ' + employee.lastName);
                  event.confirm.resolve(employee);
                  this.loadAllEmployees();
              }, error => {
                  this.toastr.error('Fail', 'Employee not deactiavated');
              });
        } else {
          event.confirm.reject();
        }
      }
    
      onCreateConfirm(event): void {
        if (window.confirm('Are you sure you want to save?')) {
          let newEmployee: Employee = new Employee();
          newEmployee.firstName =  event.newData.firstName;
          newEmployee.lastName = event.newData.lastName;
          newEmployee.title = event.newData.title;
          newEmployee.email = event.newData.email;
          newEmployee.phone = event.newData.phone;
          newEmployee.cardId = event.newData.cardId;
          newEmployee.companyId = this.currentUser.companyId;
          newEmployee.active = event.newData.active;
          this.employeeService.create(newEmployee).subscribe(
              data => {
                  event.newData.id = data.id;
                  event.confirm.resolve(event.newData);
                  this.toastr.success('Saved', 'New Employee');
              }, error => {
                  this.toastr.error('Fail', 'New Employee not saved');
              });
         // event.confirm.resolve();
    
        } else {
          event.confirm.reject();
        }
      }
    
       onEditConfirm(event): void {
        if (window.confirm('Are you sure you want to save?')) {
          let newEmployee: Employee = new Employee();
          newEmployee.id = event.data.id;
          newEmployee.firstName =  event.newData.firstName;
          newEmployee.lastName = event.newData.lastName;
          newEmployee.title = event.newData.title;
          newEmployee.email = event.newData.email;
          newEmployee.phone = event.newData.phone;
          newEmployee.cardId = event.newData.cardId;
          newEmployee.companyId = this.currentUser.companyId;
          newEmployee.active = event.newData.active;

          this.employeeService.update(newEmployee).subscribe(
              data => {
                  this.toastr.success('Employee Updated', newEmployee.firstName + ' ' + newEmployee.lastName);
                  event.confirm.resolve();
              }, error => {
                  this.toastr.error('Fail', 'Employee not updated');
              });
    
    
    
        } else {
          event.confirm.reject();
        }
      }

    
  
}