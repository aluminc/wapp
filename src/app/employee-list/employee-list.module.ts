import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { EmployeeListRoutingModule } from "./employee-list-routing.module";
import { ChartistModule } from 'ng-chartist';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { EmployeeListComponent } from "./employee-list.component";

@NgModule({
    imports: [
        CommonModule,
        EmployeeListRoutingModule,
        ChartistModule,
        NgbModule,
        MatchHeightModule,
        Ng2SmartTableModule
    ],
    exports: [],
    declarations: [
        EmployeeListComponent
    ],
    providers: [],
})
export class EmployeeListModule { }
