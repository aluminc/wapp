import { Component, OnInit } from '@angular/core';
import { User } from '../../../shared/models';

@Component({
    selector: 'app-user-profile-page',
    templateUrl: './user-profile-page.component.html',
    styleUrls: ['./user-profile-page.component.scss']
})

export class UserProfilePageComponent implements OnInit {

    //Variable Declaration
    currentPage: string = "About"
    currentUser: User;


    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // Horizontal Timeline js for user timeline
        $.getScript('./assets/js/vertical-timeline.js');
    }

    showPage(page: string) {
        this.currentPage = page;
    }
}