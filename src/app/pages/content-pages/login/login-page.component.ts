import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from '../../../shared/auth/auth.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {
    user: any = {};
    loading = false;
    returnUrl: string;

    @ViewChild('f') loginForm: NgForm;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService,) { }

        ngOnInit() {
       this.authService.logout();
         //   window.location.href = window.location.href.split('#')[0];
          }

    // On submit button click    
    onSubmit() {
        this.loading = true;
        this.authService.signinUser(this.user.email, this.user.password)
            .subscribe(
                data => {
                 //   this.notificationService.showToast('success', 'Success', 'Login successful');
                    this.router.navigate(['/']);

                },
                error => {
                 //   this.notificationService.showToast('error', 'Fail', error.status);
                    this.loading = false;
                });
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}