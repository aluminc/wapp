import { Component, ViewContainerRef } from '@angular/core';
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { WebSocketService } from './shared/services/webSocket.service';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from './shared/services/notification.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    //Set toastr container ref configuration for toastr positioning on screen
    // constructor(public toastr: ToastsManager, vRef: ViewContainerRef) {
    //     this.toastr.setRootViewContainerRef(vRef);
    // }
    constructor(private webSocketService: WebSocketService,
                public toastr: ToastrService,
                public notificationService: NotificationService)
     {
         let stompClient = this.webSocketService.connect();
        stompClient.connect({}, frame => {

      // Subscribe to notification topic
            stompClient.subscribe('/topic/notification', notifications => {

        // Update notifications attribute with the recent messsage sent from the server
              //  this.notifications = JSON.parse(notifications.body).count;
            //  if(notifications.userId === this.currentUser.id)
                //    this.toastr.success(notifications.body);
                    this.notificationService.notify(notifications);
            })
        });
    }
}