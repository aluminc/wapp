import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from '../shared/services/employee.service';
import { TimeSheetService } from '../shared/services/time-sheet.service';
import { TransactionService } from '../shared/services/transaction.service';
import { DeviceService } from '../shared/services/device.service';
import { Device } from '../shared/models/device';
import { TimeSheet } from '../shared/models/time-sheet';
import { Employee } from '../shared/models/employee';
import { Transaction } from '../shared/models/transaction';
import { NotificationService } from '../shared/services/notification.service';
import { SmartTableDatepickerRenderComponent, SmartTableDatepickerComponent } from './smart-table-datepicker/smart-table-datepicker.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})


export class DashboardComponent implements OnDestroy, OnInit{

    employees: Employee[] = [];
    timeSheets: TimeSheet[] = [];
    devices: Device[] = [];
    currentEmployee: Employee = new Employee;
    currentUser = JSON.parse(localStorage.getItem('currentUser'));
    subscription: any;
    isDelete ;
    interval: any;

    constructor(private employeeService: EmployeeService,
        private timeSheetService: TimeSheetService,
        private transactionService: TransactionService,
        private toastr: ToastrService,
        private notificationService: NotificationService,
        private deviceService: DeviceService) {
    }

    settings = {
        actions: { delete: this.isDelete},
      
        attr: {
            class: "table table-responsive"
          },
          add: {
            createButtonContent: '<i class="ft-check success font-medium-1 mr-2"></i>',
            cancelButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
            confirmCreate: true,   
          },
          edit:{
            editButtonContent: '<i class="ft-edit-2 info font-medium-1 mr-2"></i>',
            saveButtonContent: '<i class="ft-check success font-medium-1 mr-2"></i>',
            cancelButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
            confirmSave: true,
          },
          delete:{
            deleteButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
            confirmDelete: true,
          },
        columns: {
          startTime: {
            title: 'Clock In',
            type: 'custom',
            renderComponent: SmartTableDatepickerRenderComponent,
            editor: {
              type: 'custom',
              component: SmartTableDatepickerComponent,
            },
            filter: false,
          },
          finishTime: {
            title: 'Clock Out',
            type: 'custom',
            renderComponent: SmartTableDatepickerRenderComponent,
            editor: {
              type: 'custom',
              component: SmartTableDatepickerComponent,
            },
            filter: false,
          },
          totalMin: {
            title: 'Total.',
            type: 'string',
            filter: false,
            readOnly: true,
            valuePrepareFunction: (value) => this.printTotal(value),
          },
        },
      };

    ngOnInit() {
        this.loadAllEmployees();
        this.loadDevice();
        this.subscription = this.notificationService.refresh().subscribe(item => {
          this.loadAllEmployees();
          this.loadDevice();
        });
        this.calculateOnlineTime(); 
        this.interval = setInterval(() => { 
          this.calculateOnlineTime(); 
        }, 3000);
     }

     private loadDevice() {
      this.deviceService.getAllByCompany(this.currentUser.companyId).subscribe(devices => { this.devices = devices; });
    }
  
    private loadAllEmployees() {
        // if(this.currentUser.userType===1 || this.currentUser.userType===2){
            this.employeeService.getAllByCompany(this.currentUser.companyId).subscribe(employees => { this.employees = employees; });
            this.employees.sort((a, b) => {
              if (a.firstName > b.firstName) { return 1; }
              if (a.firstName < b.firstName) { return -1; }
              return 0;
            })
        // }else{
        //     this.employeeService.getById(this.currentUser.employeeId).subscribe(employee => { 
        //       this.employees = [];
        //       this.employees.push(employee); 
        //     });
       // }
    }
  
    private loadTimeSheets(employeeId: number) {
        this.timeSheetService.getAllByEmployee(employeeId).subscribe(timeSheets => {this.timeSheets = timeSheets; });
    }
  
    calculateOnlineTime(){
      let currentTime: number = (new Date).getTime();
      this.employees.forEach(element => {
        console.log(element.lastClockOut);
        if(element.lastClockOut === null && element.lastClockIn != null){
          let diff: number=currentTime - element.lastClockIn;
          element.onlineFor = diff - (4*3600000);
        }
      });
    }

     ngOnDestroy() {
         this.subscription.unsubscribe();
     }

     printDateTime(dateValue: string) {
        if (dateValue === null) {
           return '...';
        } else {
            let date = new Date(dateValue );
  
            return ('0' + (date.getUTCHours())).slice(-2) + ':' + ('0' + (date.getUTCMinutes())).slice(-2);
        }
    }
  
    printTotal(minutes: number): string{
        let hours: number = minutes/60 | 0;
        let remain: number = minutes % 60;
        return hours + " h, " + remain + " m";
    }

    onEditConfirm(event): void {
        if (window.confirm('Are you sure you want to save?')) {
          let newTimeSheet: TimeSheet = new TimeSheet();
          newTimeSheet.id = event.newData.id;
          newTimeSheet.employeeId = event.newData.employeeId;
          newTimeSheet.employeeFullName= event.newData.employeeFullName;
          newTimeSheet.startTime= event.newData.startTime;
          newTimeSheet.finishTime= event.newData.finishTime;
          newTimeSheet.comment= event.newData.comment;
          this.timeSheetService.update(newTimeSheet).subscribe(
              data => {
                  this.toastr.success('Time Sheet Updated', newTimeSheet.employeeFullName);
                  event.confirm.resolve(data);
              }, error => {
                  this.toastr.error('Fail', 'Time Sheet not updated');
              });
        } else {
          event.confirm.reject();
        }
      }
    
        onCreateConfirm(event): void {
        if (window.confirm('Are you sure you want to save new time-sheet?')) {
          let newTimeSheet: TimeSheet = new TimeSheet();
          newTimeSheet.id = event.newData.id;
          newTimeSheet.employeeId = this.currentEmployee.id;
          newTimeSheet.employeeFullName= this.currentEmployee.firstName + " " + this.currentEmployee.lastName;
          newTimeSheet.startTime= event.newData.startTime;
          newTimeSheet.finishTime= event.newData.finishTime;
          newTimeSheet.comment= event.newData.comment;
          this.timeSheetService.create(newTimeSheet).subscribe(
              data => {
                  this.toastr.success('Time Sheet Saved', newTimeSheet.employeeFullName);
                  event.confirm.resolve(data);
              }, error => {
                  this.toastr.error('Fail', 'Time Sheet not saved to database');
              });
        } else {
          event.confirm.reject();
        }
      }

    clockClick(employee: Employee) {
      let date = new Date();
      let formattedDate: string = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + (date.getDate())).slice(-2) + 'T' + ('0' + (date.getHours())).slice(-2) + ':' + ('0' + (date.getMinutes())).slice(-2) + ':00' ;
      let transaction: Transaction = new Transaction();
      transaction.employeeId = employee.id;
      transaction.companyId = this.currentUser.companyId;
      transaction.dateTime = formattedDate;
      transaction.transactionType = (employee.status === 'IN' ? 'OUT' : 'IN');
      this.transactionService.create(transaction).subscribe(
            data => {this.toastr.success('Clocked ' + transaction.transactionType , employee.firstName + ' ' + employee.lastName); },
         //data => {},   
         error => {this.toastr.error('Cannot clocked ' + transaction.transactionType, error);
              });
    //   setTimeout(() => {
    //       this.loadAllEmployees();
    //     }, 1000);
    //  this.redirect.emit(employee)
    }
    
    setEmployee(employee: Employee){
      this.currentEmployee = employee;
      this.loadTimeSheets(employee.id);
    }
}