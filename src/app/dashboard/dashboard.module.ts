import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { ChartistModule } from 'ng-chartist';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { SmartTableDatepickerComponent, SmartTableDatepickerRenderComponent } from './smart-table-datepicker/smart-table-datepicker.component'
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { DashboardComponent } from "./dashboard.component";

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ChartistModule,
        MatchHeightModule,
        Ng2SmartTableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        DashboardComponent,
        SmartTableDatepickerComponent,
        SmartTableDatepickerRenderComponent
    ],
    providers: [],
    entryComponents: [
        SmartTableDatepickerComponent,
        SmartTableDatepickerRenderComponent
    ]
})
export class DashboardModule { }
