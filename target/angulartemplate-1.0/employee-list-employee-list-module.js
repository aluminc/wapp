(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["employee-list-employee-list-module"],{

/***/ "./src/app/employee-list/employee-list-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/employee-list/employee-list-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: EmployeeListRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeListRoutingModule", function() { return EmployeeListRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _employee_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-list.component */ "./src/app/employee-list/employee-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _employee_list_component__WEBPACK_IMPORTED_MODULE_2__["EmployeeListComponent"],
        data: {
            title: 'Employee List'
        },
        children: []
    }
];
var EmployeeListRoutingModule = /** @class */ (function () {
    function EmployeeListRoutingModule() {
    }
    EmployeeListRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], EmployeeListRoutingModule);
    return EmployeeListRoutingModule;
}());



/***/ }),

/***/ "./src/app/employee-list/employee-list.component.html":
/*!************************************************************!*\
  !*** ./src/app/employee-list/employee-list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-12 col-md-12 col-lg-12\">\r\n            <div class=\"card\">\r\n                    <div class=\"card-header\">\r\n                        <h4 class=\"card-title\">Employee List</h4>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <div class=\"card-block\">\r\n                            <ng2-smart-table [settings]=\"settings\" [source]=\"employees\" (deleteConfirm)=\"onDeleteConfirm($event)\" (editConfirm)=\"onEditConfirm($event)\"\r\n                                (createConfirm)=\"onCreateConfirm($event)\"></ng2-smart-table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            \r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/employee-list/employee-list.component.scss":
/*!************************************************************!*\
  !*** ./src/app/employee-list/employee-list.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee-list/employee-list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/employee-list/employee-list.component.ts ***!
  \**********************************************************/
/*! exports provided: EmployeeListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeListComponent", function() { return EmployeeListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shared_services_employee_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/employee.service */ "./src/app/shared/services/employee.service.ts");
/* harmony import */ var _shared_models_employee__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/models/employee */ "./src/app/shared/models/employee.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmployeeListComponent = /** @class */ (function () {
    function EmployeeListComponent(employeeService, toastr) {
        var _this = this;
        this.employeeService = employeeService;
        this.toastr = toastr;
        this.employees = [];
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.settings = {
            actions: { delete: this.isDelete },
            attr: {
                class: "table table-responsive"
            },
            add: {
                createButtonContent: '<i class="ft-check success font-medium-1 mr-2"></i>',
                cancelButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="ft-edit-2 info font-medium-1 mr-2"></i>',
                saveButtonContent: '<i class="ft-check success font-medium-1 mr-2"></i>',
                cancelButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="ft-x danger font-medium-1 mr-2"></i>',
                confirmDelete: true,
            },
            columns: {
                firstName: {
                    title: 'First Name',
                    type: 'string',
                },
                lastName: {
                    title: 'Last Name',
                    type: 'string',
                },
                title: {
                    title: 'Title',
                    type: 'string',
                },
                email: {
                    title: 'E-Mail',
                    type: 'string',
                },
                phone: {
                    title: 'Phone',
                    type: 'string',
                },
                cardId: {
                    title: 'Card Id',
                    type: 'string',
                },
                active: {
                    title: 'Active',
                    valuePrepareFunction: function (value) { if (value) {
                        _this.isDelete = true;
                        return 'Active';
                    }
                    else {
                        _this.isDelete = false;
                        return 'Deleted';
                    } },
                    type: 'html',
                    editor: { type: 'list',
                        config: {
                            list: [
                                { value: 'true', title: 'Active' },
                                { value: 'false', title: 'Deleted' },
                            ],
                        } },
                    filter: {
                        type: 'list',
                        config: {
                            selectText: 'Show all',
                            list: [
                                { value: 'true', title: 'Active' },
                                { value: 'false', title: 'Deleted' },
                            ],
                        },
                    },
                },
            },
        };
    }
    EmployeeListComponent.prototype.ngOnInit = function () {
        this.loadAllEmployees();
    };
    EmployeeListComponent.prototype.ngOnChanges = function () {
        //  this.loadAllEmployees();
    };
    EmployeeListComponent.prototype.loadAllEmployees = function () {
        var _this = this;
        this.employeeService.getAllByCompany(this.currentUser.companyId).subscribe(function (employees) { _this.employees = employees; });
    };
    EmployeeListComponent.prototype.ngOnDestroy = function () {
    };
    EmployeeListComponent.prototype.printDateTime = function (dateValue) {
        if (dateValue === null) {
            return '...';
        }
        else {
            var date = new Date(dateValue);
            return ('0' + (date.getUTCHours())).slice(-2) + ':' + ('0' + (date.getUTCMinutes())).slice(-2);
        }
    };
    EmployeeListComponent.prototype.onEmployeeChange = function (e) {
        this.toastr.success(e.toString, '');
    };
    EmployeeListComponent.prototype.onDeleteConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to deactivate the employee?')) {
            var employee_1 = event.data;
            console.log(event.data.id);
            employee_1.active = false;
            this.employeeService.update(employee_1).subscribe(function (data) {
                _this.toastr.success('Employee deactiavated', employee_1.firstName + ' ' + employee_1.lastName);
                event.confirm.resolve(employee_1);
                _this.loadAllEmployees();
            }, function (error) {
                _this.toastr.error('Fail', 'Employee not deactiavated');
            });
        }
        else {
            event.confirm.reject();
        }
    };
    EmployeeListComponent.prototype.onCreateConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to save?')) {
            var newEmployee = new _shared_models_employee__WEBPACK_IMPORTED_MODULE_3__["Employee"]();
            newEmployee.firstName = event.newData.firstName;
            newEmployee.lastName = event.newData.lastName;
            newEmployee.title = event.newData.title;
            newEmployee.email = event.newData.email;
            newEmployee.phone = event.newData.phone;
            newEmployee.cardId = event.newData.cardId;
            newEmployee.companyId = this.currentUser.companyId;
            newEmployee.active = event.newData.active;
            this.employeeService.create(newEmployee).subscribe(function (data) {
                event.newData.id = data.id;
                event.confirm.resolve(event.newData);
                _this.toastr.success('Saved', 'New Employee');
            }, function (error) {
                _this.toastr.error('Fail', 'New Employee not saved');
            });
            // event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    EmployeeListComponent.prototype.onEditConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to save?')) {
            console.log(event);
            var newEmployee_1 = new _shared_models_employee__WEBPACK_IMPORTED_MODULE_3__["Employee"]();
            newEmployee_1.id = event.data.id;
            newEmployee_1.firstName = event.newData.firstName;
            newEmployee_1.lastName = event.newData.lastName;
            newEmployee_1.title = event.newData.title;
            newEmployee_1.email = event.newData.email;
            newEmployee_1.phone = event.newData.phone;
            newEmployee_1.cardId = event.newData.cardId;
            newEmployee_1.companyId = this.currentUser.companyId;
            newEmployee_1.active = event.newData.active;
            console.log(event.data);
            console.log(event.newData);
            this.employeeService.update(newEmployee_1).subscribe(function (data) {
                _this.toastr.success('Employee Updated', newEmployee_1.firstName + ' ' + newEmployee_1.lastName);
                event.confirm.resolve();
            }, function (error) {
                _this.toastr.error('Fail', 'Employee not updated');
            });
        }
        else {
            event.confirm.reject();
        }
    };
    EmployeeListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-list',
            template: __webpack_require__(/*! ./employee-list.component.html */ "./src/app/employee-list/employee-list.component.html"),
            styles: [__webpack_require__(/*! ./employee-list.component.scss */ "./src/app/employee-list/employee-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_employee_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]])
    ], EmployeeListComponent);
    return EmployeeListComponent;
}());



/***/ }),

/***/ "./src/app/employee-list/employee-list.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/employee-list/employee-list.module.ts ***!
  \*******************************************************/
/*! exports provided: EmployeeListModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeListModule", function() { return EmployeeListModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _employee_list_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-list-routing.module */ "./src/app/employee-list/employee-list-routing.module.ts");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-chartist */ "./node_modules/ng-chartist/dist/ng-chartist.js");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng_chartist__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _shared_directives_match_height_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/directives/match-height.directive */ "./src/app/shared/directives/match-height.directive.ts");
/* harmony import */ var _employee_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./employee-list.component */ "./src/app/employee-list/employee-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var EmployeeListModule = /** @class */ (function () {
    function EmployeeListModule() {
    }
    EmployeeListModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _employee_list_routing_module__WEBPACK_IMPORTED_MODULE_3__["EmployeeListRoutingModule"],
                ng_chartist__WEBPACK_IMPORTED_MODULE_4__["ChartistModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _shared_directives_match_height_directive__WEBPACK_IMPORTED_MODULE_6__["MatchHeightModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["Ng2SmartTableModule"]
            ],
            exports: [],
            declarations: [
                _employee_list_component__WEBPACK_IMPORTED_MODULE_7__["EmployeeListComponent"]
            ],
            providers: [],
        })
    ], EmployeeListModule);
    return EmployeeListModule;
}());



/***/ })

}]);
//# sourceMappingURL=employee-list-employee-list-module.js.map